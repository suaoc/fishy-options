package com.aoc.android.fo.controllers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.aoc.android.fo.R;
import com.aoc.android.fo.R.id;
import com.aoc.android.fo.R.layout;
import com.aoc.android.fo.objects.Parent;

public class MyCustomAdapter extends BaseExpandableListAdapter {

	private LayoutInflater inflater;
	private ArrayList<Parent> mParent;

	public MyCustomAdapter(Context context, ArrayList<Parent> parent) {
		mParent = parent;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getGroupCount() {
		return mParent.size();
	}

	@Override
	public int getChildrenCount(int i) {
		return mParent.get(i).getArrayChildren().size();
	}

	@Override
	public Object getGroup(int i) {
		return mParent.get(i).getTitle();
	}

	@Override
	public Object getChild(int i, int i1) {
		return mParent.get(i).getArrayChildren().get(i1);
	}

	@Override
	public long getGroupId(int i) {
		return i;
	}

	@Override
	public long getChildId(int i, int i1) {
		return i1;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
		if (view == null) {
			view = inflater.inflate(R.layout.list_item_parent, viewGroup, false);
		}
		TextView textView = (TextView) view.findViewById(R.id.list_item_text_view);
		textView.setText(getGroup(i).toString());
		return view;
	}

	@Override
	public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
		if (view == null) {
			view = inflater.inflate(R.layout.list_item_child, viewGroup, false);
		}
		TextView textView = (TextView) view.findViewById(R.id.tv_details_date);
		textView.setText(mParent.get(i).getArrayChildren().get(i1));
		return view;
	}

	@Override
	public boolean isChildSelectable(int i, int i1) {
		return true;
	}
}