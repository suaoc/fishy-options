package com.aoc.android.fo;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aoc.android.fo.objects.Vote;

public class VotesAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private List<Vote> votesList = new ArrayList<Vote>();

	VotesAdapter(Context c, List<Vote> vl, Boolean upVotes) {
		mInflater = LayoutInflater.from(c);
		votesList.clear();
		votesList.addAll(vl);
		if (upVotes != null) {
			filter(upVotes.booleanValue());
		}
	}

	private void filter(boolean upVotes) {
		if (upVotes) {
			List<Vote> rem = new ArrayList<Vote>();
			for (Vote v : votesList) {
				if (!v.thumbs_up) {
					rem.add(v);
				}
			}
			votesList.removeAll(rem);
		} else if (!upVotes) {
			List<Vote> rem = new ArrayList<Vote>();
			for (Vote v : votesList) {
				if (v.thumbs_up) {
					rem.add(v);
				}
			}
			votesList.removeAll(rem);
		}
	}

	@Override
	public int getCount() {
		return votesList.size();
	}

	@Override
	public Object getItem(int position) {
		return votesList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		Vote v = votesList.get(position);

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.vote_row, null);
			holder = new ViewHolder();
			holder.username = (TextView) convertView.findViewById(R.id.vote_username);
			holder.date = (TextView) convertView.findViewById(R.id.vote_date);
			holder.comment = (TextView) convertView.findViewById(R.id.vote_comment);
			holder.thumbImage = (ImageView) convertView.findViewById(R.id.image_thumb);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.username.setText(v.username);
		holder.date.setText(v.voted_on.substring(0, v.voted_on.indexOf("T")));
		holder.comment.setText(v.comments);
		if (v.thumbs_up) {
			holder.thumbImage.setBackgroundResource(R.drawable.thumbsup_flip_prsd);

		} else {
			holder.thumbImage.setBackgroundResource(R.drawable.thumsdwn_prsd);
		}
		return convertView;
	}

	class ViewHolder {
		TextView username = null, date = null, comment = null;
		ImageView thumbImage = null;
	}
}
/*
 * 
 * { "comments": "test comment", "id": 45008, "resource_uri":
 * "/api/v1/vote/45008/", "thumbs_up": false, "user": "/api/v1/user/1/",
 * "userid": 1, "username": "admin", "voted_on": "2013-01-13T15:56:22" },
 */