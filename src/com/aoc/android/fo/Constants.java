package com.aoc.android.fo;

import java.util.regex.Pattern;

public class Constants {

	public static final boolean LOG = true;
	public static final int USERNAME_MIN_LENGTH = 3;
	public static final int USERNAME_MAX_LENGTH = 20;
	public static final int PASSWORD_MIN_LENGTH = 4;
	public static final int PASSWORD_MAX_LENGTH = 15;

	public static final Pattern EMAIL_PATTERN = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
			+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");

	public static final String MSG_NO_INTERNET = "No internet connection found!";
	public static final String MSG_ERROR_UNKNOWN = "An error occurred. Please try again.";
	public static final String MSG_ERROR_UNAUTHORIZED = "Invalid username/password";
	public static final String MSG_ERROR_INVALID_USERNAME = "Username must be " + USERNAME_MIN_LENGTH + "-"
			+ USERNAME_MAX_LENGTH + " alphanumeric characters!";
	public static final String MSG_ERROR_INVALID_PASSWORD = "Password must be " + PASSWORD_MIN_LENGTH + "-"
			+ PASSWORD_MAX_LENGTH + " alphanumeric characters!";
	public static final String MSG_ERROR_INVALID_EMAIL = "Please enter a valid email address";
	public static final String MSG_POSTED_SUCCESSFULLY = "Added successfully";
	public static final String MSG_PLZ_WAIT = "Please wait...";
	public static final String MSG_EMAIL_SENT = "Email sent! Please check your inbox.";

	public static final String URL_BASE = "http://fishyoptionsapp.appspot.com/";
	public static final String URL_LOGIN = "api/v1/user/login/?format=json";
	public static final String URL_SIGN_UP = "api/v1/user/signup/?format=json";
	public static final String URL_CHANGE_PW = "api/v1/user/changepassword/?format=json";

	public static final String URL_Follow = "http://fishyoptionsapp.appspot.com/api/v1/follower/";
	public static final String URL_MyList = "http://fishyoptionsapp.appspot.com/api/v1/follower/?format=json&limit=3&user=";
	public static final String URL_FORGOT_PASSWORD = "api/v1/user/forgotpassword/?format=json";
	public static final String URL_TRADE = "api/v1/trade/?format=json&limit=3&live=true";
	public static final String URL_VOTE = "http://fishyoptionsapp.appspot.com/api/v1/vote/";
	public static final String URL_SEARCH = "http://fishyoptionsapp.appspot.com/api/v1/trade/?stk__startswith=%s&format=json";
	public static final String URL_DELETE = "http://fishyoptionsapp.appspot.com/api/v1/follower/%1$s/?username=%2$s&api_key=%3$s";

	public static final int REQUEST_CODE_LOGIN = 101;
	public static final int REQUEST_CODE_SIGNUP = 102;
	public static final int RESULT_CODE_LOGIN_SUCCESS = 201;
	public static final int RESULT_CODE_SIGNUP_SUCCESS = 202;
	public static final int RESULT_CODE_LOGOUT = 211;

	public static final int SHOW_DIALOG = 101;
	public static final int DISMISS_DIALOG = 102;
}
