package com.aoc.android.fo;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.aoc.android.fo.objects.Option;

public class HighLowActivity extends Activity {
	LayoutParams lpfw = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
	LinearLayout parentLayout = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_high_low);

		parentLayout = (LinearLayout) findViewById(R.id.parent_high_low);

		boolean fromMyList = getIntent().getExtras().getBoolean("fromMyList");
		List<Option> options = null;
		if (!fromMyList) {
			log("selected index is: " + FishyOptionsActivity.selectedItem);
			log("options size is: " + FishyOptionsActivity.tdo.objects.size());
			if (FishyOptionsActivity.selectedItem >= FishyOptionsActivity.tdo.objects.size()) {
				log("index out of bounds for tdo");
				return;
			}
			options = FishyOptionsActivity.tdo.objects.get(FishyOptionsActivity.selectedItem).getOptions();
		} else {
			log("selected index is: " + MyListActivity.selectedItem);
			log("options size is: " + MyListActivity.mld.objects.size());
			if (FishyOptionsActivity.selectedItem >= MyListActivity.mld.objects.size()) {
				log("index out of bounds for mld");
				return;
			}
			options = MyListActivity.mld.objects.get(MyListActivity.selectedItem).getTrade().getOptions();
		}
		if (options == null) {
			return;
		}
		for (Option option : options) {
			log("High/Low for " + option.symbol);
			if (option.highest == null || option.lowest == null) {
				continue;
			}
			if (option.highest.high == -1 && option.lowest.low == -1) {
				continue;
			}
			// show highest
			log("Highest: " + option.highest.high + " on " + option.highest.date);
			log("Lowest: " + option.lowest.low + " on " + option.lowest.date);

			addRow(option.symbol, option.highest.high + "", option.highest.date, option.lowest.low + "",
					option.lowest.date);
		}
		if (options.size() <= 0) {
			showNoDataFound();
			log("options size is less than 1");
		}
	}

	private void addRow(String osymbol, String hVal, String hDate, String lVal, String lDate) {
		LayoutParams lpfw = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		LinearLayout highLowOne = (LinearLayout) getLayoutInflater().inflate(R.layout.high_low_one, null);

		((TextView) highLowOne.findViewById(R.id.option_symbol)).setText(osymbol);
		((TextView) highLowOne.findViewById(R.id.value_highest)).setText(hVal);
		((TextView) highLowOne.findViewById(R.id.value_lowest)).setText(lVal);
		((TextView) highLowOne.findViewById(R.id.date_highest)).setText(hDate);
		((TextView) highLowOne.findViewById(R.id.date_lowest)).setText(lDate);

		parentLayout.addView(highLowOne, lpfw);
	}

	private void showNoDataFound() {
		parentLayout.removeAllViews();
		TextView t = new TextView(this);
		t.setText("No data found!");
		log("No Data Found");
		t.setGravity(Gravity.CENTER);
		lpfw.setMargins(0, 20, 0, 0);
		parentLayout.addView(t, lpfw);
	}

	private void log(String msg) {
		Utils.Logger.error("OptionsDetails", msg);
	}

}
