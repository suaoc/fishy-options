package com.aoc.android.fo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.widget.Toast;

public class BackgroundWorker extends AsyncTask<String, Void, String> {

	private CustomSpinner cSpinner = null;
	private NetworkActivity nActivity = null;
	private JSONObject userObject = null;
	private String errorMsg = null;
	private boolean isGet = true;
	private boolean showIndicator = false;
	private boolean created = false; // handle post with no response body

	public BackgroundWorker(NetworkActivity ba, JSONObject uo, boolean get, boolean indicator) {
		this.nActivity = ba;
		this.userObject = uo;
		this.isGet = get;
		this.showIndicator = indicator;
	}

	@Override
	protected void onPreExecute() {
		// -- check internet connection
		if (!Utils.checkInternet(nActivity.getContext())) {
			Toast.makeText(nActivity.getContext(), Constants.MSG_NO_INTERNET, Toast.LENGTH_SHORT).show();
			this.cancel(true);
			return;
		}
		if (this.showIndicator) {
			cSpinner = CustomSpinner.show(nActivity.getContext(), "", Constants.MSG_PLZ_WAIT, true, false, null);
		}
	}

	@Override
	protected void onPostExecute(String result) {
		if (this.created) {
			Toast.makeText(nActivity.getContext(), Constants.MSG_POSTED_SUCCESSFULLY, Toast.LENGTH_SHORT).show();
			nActivity.parseResponse(result);
			return;
		}
		if (result != null) {
			nActivity.parseResponse(result);
		} else {
			log("Response is null");
			if (errorMsg != null) {
				Utils.createInfoDialog(nActivity.getContext(), "", errorMsg).show();
				errorMsg = null;
			}
		}
		if (this.showIndicator) {
			dismissSpinner();
		}
	}

	@Override
	protected String doInBackground(String... params) {
		log("URL: " + params[0]);
		if (this.isGet) {
			return doGet(params[0]);
		} else {
			return doPost(params[0]);
		}
	}

	private String doGet(String url) {
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet(url);
			HttpResponse response = client.execute(request);
			// Here i try to read the response
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			final StringBuilder sb = new StringBuilder();
			final String sep = System.getProperty("line.separator");
			String line = "";
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(sep);
			}
			return sb.toString();
		} catch (HttpHostConnectException hhce) {
			log("HttpHostConnectionException: " + hhce.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			errorMsg = Constants.MSG_ERROR_UNKNOWN;
		}
		return null;
	}

	private String doPost(String url) {
		log(url);
		String response = null;
		DefaultHttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		try {
			if (userObject != null) {
				// log(userObject.toString());
				StringEntity se = new StringEntity(userObject.toString());
				post.setEntity(se);
			}
			post.setHeader("Content-Type", "application/json");
			post.setHeader("Accept", "application/json");
		} catch (UnsupportedEncodingException uee) {
			uee.printStackTrace();
		}
		try {
			ResponseHandler<String> responseHandler = new BasicResponseHandler() {
				@Override
				public String handleResponse(HttpResponse response) throws HttpResponseException, IOException {
					log(response.getStatusLine().getStatusCode() + "");
					if (!BackgroundWorker.this.showIndicator && response.getStatusLine().getStatusCode() == 201) {
						// it is for votes
						log("created");
						created = true;
						errorMsg = null;
					}
					return super.handleResponse(response);
				}
			};
			response = client.execute(post, responseHandler);
			log("Response is: " + response.toString());
		} catch (HttpResponseException hre) {
			if (hre.getStatusCode() == 401) {
				errorMsg = Constants.MSG_ERROR_UNAUTHORIZED;
			}
			log("hre");
		} catch (ClientProtocolException cpe) {
			cpe.printStackTrace();
			response = null;
			errorMsg = Constants.MSG_ERROR_UNKNOWN;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			log("IOException occurred, returning..");
			response = null;
			errorMsg = Constants.MSG_ERROR_UNKNOWN;
		}

		return response;
	}

	private void dismissSpinner() {
		if (cSpinner != null && cSpinner.isShowing()) {
			cSpinner.dismiss();
		}
	}

	private void log(String msg) {
		Utils.Logger.error("BackgroundWorker", msg);
	}

}
