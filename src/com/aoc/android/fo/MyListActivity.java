package com.aoc.android.fo;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.gson.Gson;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.aoc.android.fo.MyListAdapter.TagDelete;

public class MyListActivity extends Activity implements NetworkActivity {

	public static MyListData mld = null;
	ListView list;
	MyListAdapter mla;
	Button loadMore;
	String next;
	TextView message;
	public static int selectedItem = -1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mylist);
		list = (ListView) findViewById(R.id.list);
		message = (TextView) findViewById(R.id.message);
		loadMore = (Button) findViewById(R.id.loadmore);
		// if (mld == null || mld.getmy_list().size() == 0) {

		BackgroundWorker bw = new BackgroundWorker(this, null, true, true);
		bw.execute(Constants.URL_MyList + LoginActivity.sessionTokens.get("id"));
		// } else {
		// setListAdapter();

		// }

		loadMore.setVisibility(View.INVISIBLE);
		loadMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (mld.getMeta().next != null
						|| !mld.getMeta().next.equals("")) {
					BackgroundWorker bwc = new BackgroundWorker(
							MyListActivity.this, null, true, true);
					bwc.execute(Constants.URL_BASE + mld.getMeta().next);

				}
			}
		});

	}

	public void setText() {
		message.setText("No items in your list.");
	}

	public void myClickHandler(View v) {

		ImageButton b = (ImageButton) v;

		int posToDelete = ((TagDelete) b.getTag()).position;
		int followerid = ((TagDelete) b.getTag()).follower;

		String url = String.format(Constants.URL_DELETE, followerid,
				LoginActivity.sessionTokens.get("user_name"),
				LoginActivity.sessionTokens.get("api_key"));
		FishyOptionsActivity.log(url);
		deleteItem(posToDelete, url);
		if (mld.getMeta().next == null && mld.getmy_list().size() == 0) {
			setText();
		}

	}

	public void setListAdapter() {
		//list.setTextFilterEnabled(true);
		mla = new MyListAdapter(this, mld.getmy_list());
		list.setAdapter(mla);
		if (mld.getMeta().next == null) {
			loadMore.setVisibility(View.INVISIBLE);
		}
		else
			loadMore.setVisibility(View.VISIBLE);
		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
				
				FishyOptionsActivity.log("clicking item");
				selectedItem = pos;
				Intent i = new Intent(MyListActivity.this, OptionDetailTabs.class);
				i.putExtra("fromMyList", true);
				startActivity(i);
			}
		});

	}

	@Override
	public Context getContext() {
		return this;
	}

	@Override
	public void parseResponse(String jsonResponse) {
		try {
			FishyOptionsActivity.log(jsonResponse);
			Gson gson = new Gson();
			Reader r = new InputStreamReader(new ByteArrayInputStream(
					jsonResponse.getBytes()));

			MyListData listObj = gson.fromJson(r, MyListData.class);

			if (listObj != null) {
				if (listObj.getmy_list() == null
						|| listObj.getmy_list().size() == 0) {
					setText();
					// this.finish();
				}
				if (mld != null) {
					for (FollowingData temp : listObj.getmy_list()) {
						mld.getmy_list().add(temp);
					}
					mld.setMeta(listObj.getMeta());
				} else
					mld = listObj;

				setListAdapter();
			}

		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	@Override
	public void finish() {
		mld = null;
		super.finish();

	}

	public void deleteItem(final int pos, String u) {
		/*String response = null;
		DefaultHttpClient client = new DefaultHttpClient();
		HttpDelete delete = new HttpDelete(u);
		String errorMsg = "";
		try {

			delete.setHeader("Accept", "application/json");
		} catch (Exception uee) {
			uee.printStackTrace();
		}

		try {
			ResponseHandler<String> responseHandler = new BasicResponseHandler() {
				@Override
				public String handleResponse(HttpResponse response)
						throws HttpResponseException, IOException {
					//FishyOptionsActivity.log(response.getStatusLine()
						//	.getStatusCode() + "");
					if (response.getStatusLine().getStatusCode() == 204) {
						// it is for votes
						FishyOptionsActivity.log("deleted");
						mld.getmy_list().remove(pos);
						mla.notifyDataSetChanged();
						Toast.makeText(MyListActivity.this, "Selected item deleted.", Toast.LENGTH_SHORT).show();
						if (mld.getMeta().next != null) {
							String next_url = mld.getMeta().next;
							int offset = mld.getMeta().offset;
							int limit = mld.getMeta().limit;
							//if (offset + limit - 1 > mld.getMeta().total_count) {
								int index=next_url.indexOf("offset="+(offset + limit));
								next_url=next_url.replace("offset=" + (offset + limit),
										"offset=" + (offset + limit - 1));
								mld.getMeta().offset--;
								mld.getMeta().next = next_url;
								//FishyOptionsActivity.log("new next url "
									//	+ mld.getMeta().next);
								//FishyOptionsActivity.log("index is "
								//		+ index);
								//FishyOptionsActivity.log("offset="+(offset + limit));
							//}
						}
						// created = true;
					}
					return super.handleResponse(response);
				}
			};
			response = client.execute(delete, responseHandler);
			// FishyOptionsActivity.log("Response is: " + response.toString());
		} catch (HttpResponseException hre) {
			if (hre.getStatusCode() == 401) {
				errorMsg = Constants.MSG_ERROR_UNAUTHORIZED;
			}
			FishyOptionsActivity.log("hre");
		} catch (ClientProtocolException cpe) {
			cpe.printStackTrace();
			response = null;
			errorMsg = Constants.MSG_ERROR_UNKNOWN;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			FishyOptionsActivity.log("IOException occurred, returning..");
			response = null;
			errorMsg = Constants.MSG_ERROR_UNKNOWN;
		}*/
		new DeleteBackground(pos).execute(u);
	}

	private class DeleteBackground extends AsyncTask<String, String, String> {
		
		int pos;
		public DeleteBackground(int p) {
			pos=p;
		}

		ResponseHandler<String> responseHandler = new BasicResponseHandler() {
			@Override
			public String handleResponse(HttpResponse response)
					throws HttpResponseException, IOException {
				//FishyOptionsActivity.log(response.getStatusLine()
					//	.getStatusCode() + "");
				if (response.getStatusLine().getStatusCode() == 204) {
					// it is for votes
					FishyOptionsActivity.log("deleted");
					mld.getmy_list().remove(pos);
					runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							mla.notifyDataSetChanged();
							Toast.makeText(MyListActivity.this, "Selected item deleted.", Toast.LENGTH_SHORT).show();
							
						}
					});
					
					if (mld.getMeta().next != null) {
						String next_url = mld.getMeta().next;
						int offset = mld.getMeta().offset;
						int limit = mld.getMeta().limit;
						//if (offset + limit - 1 > mld.getMeta().total_count) {
							int index=next_url.indexOf("offset="+(offset + limit));
							next_url=next_url.replace("offset=" + (offset + limit),
									"offset=" + (offset + limit - 1));
							mld.getMeta().offset--;
							mld.getMeta().next = next_url;
							//FishyOptionsActivity.log("new next url "
								//	+ mld.getMeta().next);
							//FishyOptionsActivity.log("index is "
							//		+ index);
							//FishyOptionsActivity.log("offset="+(offset + limit));
						//}
					}
					// created = true;
				}
				return super.handleResponse(response);
			}
		};
        @Override
        protected String doInBackground(String... params) {
        	
        	String response = null;
    		DefaultHttpClient client = new DefaultHttpClient();
    		HttpDelete delete = new HttpDelete(params[0]);
    		String errorMsg = "";
            boolean result = false;

            try{
            response = client.execute(delete,responseHandler);
            }
			// FishyOptionsActivity.log("Response is: " + response.toString());
		 catch (HttpResponseException hre) {
			if (hre.getStatusCode() == 401) {
				errorMsg = Constants.MSG_ERROR_UNAUTHORIZED;
			}
			FishyOptionsActivity.log("hre");
		} catch (ClientProtocolException cpe) {
			cpe.printStackTrace();
			response = null;
			errorMsg = Constants.MSG_ERROR_UNKNOWN;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			FishyOptionsActivity.log("IOException occurred, returning..");
			response = null;
			errorMsg = Constants.MSG_ERROR_UNKNOWN;
		}
            return response;
        }

       
    }
}
