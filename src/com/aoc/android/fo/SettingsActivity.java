package com.aoc.android.fo;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsActivity extends Activity implements NetworkActivity, OnClickListener {

	SharedPreferences mPreferences = null;
	EditText fnField = null, lnField = null, emField = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		mPreferences = getSharedPreferences("creds", MODE_PRIVATE);

		Button saveDetailsBtn = (Button) findViewById(R.id.btn_save_details);
		Button logoutBtn = (Button) findViewById(R.id.btn_logout);
		Button changePwdBtn = (Button) findViewById(R.id.btn_change_pwd);
		saveDetailsBtn.setOnClickListener(this);
		changePwdBtn.setOnClickListener(this);
		logoutBtn.setOnClickListener(this);

		fnField = ((EditText) findViewById(R.id.first_name_field));
		fnField.setText(mPreferences.getString("fn", ""));

		lnField = ((EditText) findViewById(R.id.last_name_field));
		lnField.setText(mPreferences.getString("ln", ""));

		emField = ((EditText) findViewById(R.id.email_field));
		emField.setText(mPreferences.getString("em", ""));
	}

	@Override
	public Context getContext() {
		return this;
	}

	public void parseResponse(String jsonResponse) {
		if (jsonResponse == null) {
			Utils.createInfoDialog(this, "", Constants.MSG_ERROR_UNKNOWN).show();
			return;
		}
		String msg = null;
		try {
			JSONObject sessionObject = new JSONObject(jsonResponse);
			if (sessionObject.has("success")) {
				msg = sessionObject.getString("reason");
			} else {
				// some error occurred
				msg = Constants.MSG_ERROR_UNKNOWN;
			}
		} catch (JSONException je) {
			// json parse failed
			msg = Constants.MSG_ERROR_UNKNOWN;
		} catch (Exception e) {
			e.printStackTrace();
			msg = Constants.MSG_ERROR_UNKNOWN;
		}
		if (msg != null) {
			Utils.createInfoDialog(this, "", msg).show();
		}
	}

	private void log(String msg) {
		Utils.Logger.error("Settings Activity", msg);
	}

	/**************** Button handlers *******************/
	public void saveBtnClicked() {
		log("Save Details clicked");
		if (fnField == null || lnField == null || emField == null) {
			return;
		}
		String firstName = fnField.getText().toString();
		String lastName = lnField.getText().toString();
		String emailAddr = emField.getText().toString();

		if (Utils.isEmptyString(firstName)) {
			Utils.createInfoDialog(this, "", "Invalid first name!").show();
			return;
		}
		if (Utils.isEmptyString(lastName)) {
			Utils.createInfoDialog(this, "", "Invalid last name!").show();
			return;
		}
		if (!Utils.isValidEmail(emailAddr)) {
			Utils.createInfoDialog(this, "", Constants.MSG_ERROR_INVALID_EMAIL).show();
			return;
		}
		Editor editor = mPreferences.edit().putString("fn", firstName).putString("ln", lastName)
				.putString("em", emailAddr);
		editor.commit();
		Toast.makeText(this, "Settings saved!", Toast.LENGTH_SHORT).show();
	}

	public void changePwdBtnClicked() {
		// curl -v -H "Accept: application/json" -H
		// "Content-type: application/json" -X POST -d '{"username":"admin",
		// "current_password":"admin", "new_password":"admin123"}'
		// http://fishyoptionsapp.appspot.com/api/v1/user/changepassword/?format=json
		JSONObject uo = new JSONObject();
		try {
			String un = mPreferences.getString("user_name", null);

			if (un == null) {
				log("username is null");
				return;
			}
			EditText cpwField = (EditText) findViewById(R.id.current_pwd_field);
			EditText npwField = (EditText) findViewById(R.id.new_pwd_field);
			EditText cnpwField = (EditText) findViewById(R.id.confirm_pwd_field);

			String cpwStr = cpwField.getText().toString();
			String npwStr = npwField.getText().toString();
			String cnpwStr = cnpwField.getText().toString();

			if (!npwStr.equals(cnpwStr)) {
				Utils.createInfoDialog(this, "", "New passwords don't match!").show();
				return;
			}

			uo.put("username", un);
			uo.put("current_password", cpwStr);
			uo.put("new_password", npwStr);
		} catch (JSONException je) {
			je.printStackTrace();
		}
		BackgroundWorker bw = new BackgroundWorker(this, uo, false, true);
		bw.execute(Constants.URL_BASE + Constants.URL_CHANGE_PW);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_save_details:
			saveBtnClicked();
			break;
		case R.id.btn_change_pwd:
			changePwdBtnClicked();
			break;
		case R.id.btn_logout:
			this.setResult(Constants.RESULT_CODE_LOGOUT);
			this.finish();
			break;
		default:
			break;
		}

	}
}
