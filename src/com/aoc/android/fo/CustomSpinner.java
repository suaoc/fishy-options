package com.aoc.android.fo;

import android.app.Dialog;
import android.content.Context;
import android.view.ViewGroup.LayoutParams;
import android.widget.ProgressBar;

public class CustomSpinner extends Dialog {

	public static CustomSpinner show(Context c, CharSequence title, CharSequence msg, boolean indeterminate,
			boolean cancelable, OnCancelListener cancelListener) {
		CustomSpinner dialog = new CustomSpinner(c);
		dialog.setTitle(title);
		dialog.setCancelable(cancelable);
		dialog.setOnCancelListener(cancelListener);
		/* The next line will add the ProgressBar to the dialog. */
		dialog.addContentView(new ProgressBar(c),
				new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		dialog.show();

		return dialog;
	}

	public CustomSpinner(Context context) {
		super(context, R.style.CustomDialog);
	}
}