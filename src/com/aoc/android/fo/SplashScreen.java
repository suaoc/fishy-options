package com.aoc.android.fo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SplashScreen extends Activity {

	private Thread mSplashThread;
	public static final String PREFS_FILE_NAME = "mobileuidata";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// try {
		/*
		 * If resources for a specific screen size are not provided, just finish
		 * the application
		 */
		setContentView(R.layout.static_splash);
		// } catch (Exception e) {
		// finish();
		// return;
		// }

		this.setTitle(R.string.app_name);

		// The thread to wait for splash screen events
		mSplashThread = new Thread() {
			@Override
			public void run() {
				try {
					synchronized (this) {
						wait(2000);
					}
				} catch (InterruptedException ex) {
				}
				finish();

				Intent i = new Intent(SplashScreen.this, TermsConditions.class);
				startActivity(i);
			}
		};
		mSplashThread.start();
	}
}