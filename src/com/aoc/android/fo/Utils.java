package com.aoc.android.fo;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Utils {

	public static boolean checkInternet(Context c) {
		ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifi != null && wifi.isConnected()) {
			return true;
		}
		NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (mobile != null && mobile.isConnected()) {
			return true;
		}
		NetworkInfo active = cm.getActiveNetworkInfo();
		if (active != null && active.isConnected()) {
			return true;
		}
		return false;
	}

	public static Dialog createInfoDialog(Context c, String title, String message) {
		final Dialog d = new Dialog(c, R.style.CustomDialog);
		d.setContentView(R.layout.dialog_info);
		final TextView commentField = (TextView) d.findViewById(R.id.tv_info_msg);
		commentField.setText(message);
		Button btnPost = (Button) d.findViewById(R.id.btn_ok);
		btnPost.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				d.dismiss();
			}
		});
		return d;
	}

	public static Dialog createConfirmExitDialog(final Activity a) {
		final Dialog d = new Dialog(a, R.style.CustomDialog);
		d.setContentView(R.layout.dialog_confirm_exit);
		Button btnYes = (Button) d.findViewById(R.id.btn_yes);
		btnYes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				d.dismiss();
				a.finish();
			}
		});
		Button btnNo = (Button) d.findViewById(R.id.btn_no);
		btnNo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				d.dismiss();
			}
		});
		return d;
	}

	public static boolean isValidEmail(String email) {
		return Constants.EMAIL_PATTERN.matcher(email).matches();
	}

	public static boolean isValidString(String str, int minLimit, int maxLimit) {
		if (isEmptyString(str)) {
			return false;
		}
		if (str.length() < minLimit || str.length() > maxLimit) {
			return false;
		}
		return true;
	}

	public static boolean isEmptyString(String str) {
		if (str == null || str.length() <= 0) {
			return true;
		}
		return false;
	}

	public static class Logger {
		public static void error(String tag, String msg) {
			if (Constants.LOG) {
				Log.e(tag, msg);
			}
		}

		public static void debug(String tag, String msg) {
			if (Constants.LOG) {
				Log.d(tag, msg);
			}
		}

		public static void info(String tag, String msg) {
			if (Constants.LOG) {
				Log.i(tag, msg);
			}
		}
	}
}
