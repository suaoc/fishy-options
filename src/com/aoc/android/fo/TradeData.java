package com.aoc.android.fo;

import java.util.ArrayList;

import com.aoc.android.fo.objects.Trade;

public class TradeData {
	ArrayList<Trade> objects;
	private Meta meta;

	public Meta getMeta() {
		return meta;
	}

	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	public ArrayList<Trade> getObjects() {
		return objects;
	}

	public void setObjects(ArrayList<Trade> objects) {
		this.objects = objects;
	}

	@SuppressWarnings("unused")
	public class Meta {

		public int limit;
		public String next;
		public int offset;
		public String previous;
		public int total_count;

	}

}
