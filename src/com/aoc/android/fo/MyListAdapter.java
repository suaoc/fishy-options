package com.aoc.android.fo;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aoc.android.fo.objects.Option;
import com.aoc.android.fo.objects.Trade;

public class MyListAdapter extends BaseAdapter implements Filterable {

	ArrayList<FollowingData> TD = null;
	private static LayoutInflater inflater = null;
	Activity activity;
	TextView tradeName;
	TextView followers;
	TextView date;
	TextView upText;
	TextView downText;
	TextView call1;
	TextView call2;
	ImageView category;
	private ImageView delete;

	MyListAdapter(Activity a, ArrayList<FollowingData> td) {
		this.activity = a;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.TD = td;
	}

	@Override
	public int getCount() {
		return TD.size();
	}

	@Override
	public Object getItem(int position) {
		return TD.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.my_list_item, null);
		tradeName = (TextView) vi.findViewById(R.id.trade_name);
		followers = (TextView) vi.findViewById(R.id.followers);
		date = (TextView) vi.findViewById(R.id.date);
		upText = (TextView) vi.findViewById(R.id.uptext);
		downText = (TextView) vi.findViewById(R.id.downtext);
		call1 = (TextView) vi.findViewById(R.id.call1);
		call2 = (TextView) vi.findViewById(R.id.call2);
		category = (ImageView) vi.findViewById(R.id.category);
		delete = (ImageView) vi.findViewById(R.id.delete);
		//RelativeLayout up_rl = (RelativeLayout) vi.findViewById(R.id.up);
		//RelativeLayout down_rl = (RelativeLayout) vi.findViewById(R.id.down);

		Trade t = TD.get(position).getTrade();
		tradeName.setText(t.getStk());

		List<Option> opt = t.getOptions();
		String call1_text = "";
		String call2_text = "";
		if (opt != null && opt.size() > 0) {
			Option o = opt.get(0);
			call1_text = o.getAction() + " " + o.getQty() + " "
					+ ResolveMonth.resolve(o.getMonth()) + " " + o.getStrike()
					+ " " + o.getType();

			if (opt.size() > 1) {
				o = opt.get(0);
				call2_text = o.getAction() + " " + o.getQty() + " "
						+ ResolveMonth.resolve(o.getMonth()) + " "
						+ o.getStrike() + " " + o.getType();

			}

		}
		String date_text = null;
		FishyOptionsActivity.log(t.getStk());
		if (t.getIdentified_on() != null || !t.getIdentified_on().equals("")) {
			String temp[] = t.getIdentified_on().split("T")[0].split("-");
			date_text = ResolveMonth.resolve(Integer.parseInt(temp[1])) + "-"
					+ temp[2];
		}
		String follower_text = "0 Followers";
		if (t.getFollowers() != null && t.getFollowers().size() != 0) {
			follower_text = t.getFollowers().size() + " Followers";
		}
		if (t.getCategory().equalsIgnoreCase("Bull")) {
			category.setBackgroundColor(Color.RED);
		} else if (t.getCategory().equalsIgnoreCase("Bear")) {
			category.setBackgroundColor(Color.GREEN);
		}

		call1.setText(call1_text);
		call2.setText(call2_text);
		date.setText(date_text);
		followers.setText(follower_text);
		upText.setText(t.getVote_up_count() + "");
		downText.setText(t.getVote_down_count() + "");
		TagDelete tag = new TagDelete();
		tag.follower = TD.get(position).getId();
		tag.position = position;
		delete.setTag(tag);

		int height = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, t.getVote_up_count() * 2, activity
						.getResources().getDisplayMetrics());
		int height2 = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, t.getVote_down_count() * 2,
				activity.getResources().getDisplayMetrics());
		// int baseHeight=rl.getHeight();
		if (height > -1 && height < 120) {
			// FishyOptionsActivity.log(41 + height + " height of up vote of "
			// + t.getStk());
			/*
			 * RelativeLayout.LayoutParams params = new
			 * RelativeLayout.LayoutParams(
			 * RelativeLayout.LayoutParams.WRAP_CONTENT, 40 + height);
			 * params.addRule(RelativeLayout.ALIGN_LEFT, followers.getId());
			 * params.addRule(RelativeLayout.BELOW, followers.getId());
			 * up_rl.setLayoutParams(params);
			 */
			TextView up = (TextView) vi.findViewById(R.id.uptext);
			up.setHeight(10 + height);

		}

		if (height2 > -1 && height2 < 120) {
			// FishyOptionsActivity.log(41 + height2 +
			// " height of down vote of "
			// + t.getStk());
			/*
			 * RelativeLayout.LayoutParams params = new
			 * RelativeLayout.LayoutParams(
			 * RelativeLayout.LayoutParams.WRAP_CONTENT, 41 + height2);
			 * params.addRule(RelativeLayout.BELOW, followers.getId());
			 * params.addRule(RelativeLayout.RIGHT_OF, up_rl.getId());
			 * params.setMargins(5, 0, 0, 0); down_rl.setLayoutParams(params);
			 */
			TextView down = (TextView) vi.findViewById(R.id.downtext);
			down.setHeight(10 + height2);
		}

		return vi;
	}

	public class TagDelete {

		public int position;
		public int follower;
	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		return null;
	}

}
