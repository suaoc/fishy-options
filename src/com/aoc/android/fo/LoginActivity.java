package com.aoc.android.fo;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

public class LoginActivity extends Activity implements NetworkActivity {

	String un = null, pw = null;
	public static HashMap<String, Object> sessionTokens = null;
	SharedPreferences mPreferences = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		// final Button loginButton = (Button) findViewById(R.id.btn_login);
		mPreferences = getSharedPreferences("creds", MODE_PRIVATE);
		sessionTokens = new HashMap<String, Object>();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case Constants.REQUEST_CODE_SIGNUP:
			if (resultCode == Constants.RESULT_CODE_SIGNUP_SUCCESS) {
				this.setResult(Constants.RESULT_CODE_LOGIN_SUCCESS);
				this.finish();
			}
			break;

		default:
			break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public Context getContext() {
		return this;
	}

	private boolean authenticate() {
		if (!this.validateFields()) {
			return false;
		}

		JSONObject uo = new JSONObject();
		try {
			uo.put("username", un);
			uo.put("password", pw);
		} catch (JSONException je) {
			je.printStackTrace();
		}
		BackgroundWorker bw = new BackgroundWorker(this, uo, false, true);
		bw.execute(Constants.URL_BASE + Constants.URL_LOGIN);
		return true;
	}

	public void parseResponse(String jsonResponse) {
		try {
			if (jsonResponse != null) {
				JSONObject sessionObject = new JSONObject(jsonResponse);
				if (sessionObject.has("success")) {
					boolean success = sessionObject.getBoolean("success");
					sessionTokens.put("success", success);
					if (success) {
						log(sessionObject.toString());
						int id = sessionObject.getInt("id");
						String u = sessionObject.getString("username");
						String ak = sessionObject.getString("api_key");
						String fn = sessionObject.getString("first_name");
						String ln = sessionObject.getString("last_name");
						String em = sessionObject.getString("email");
						sessionTokens.put("id", id);
						sessionTokens.put("api_key", ak);
						sessionTokens.put("user_name", u);
						sessionTokens.put("first_name", fn);
						sessionTokens.put("last_name", ln);
						sessionTokens.put("email", em);
						handleRememberBtn(id, u, em, ak, fn, ln);
						this.setResult(Constants.RESULT_CODE_LOGIN_SUCCESS);
						this.finish();
						return;
					} else {
						// login failed due to some reason
						log(sessionObject.getString("reason"));
						// sessionTokens already have "success" flag
						sessionTokens.put("errorMsg", sessionObject.getString("reason"));
					}
				} else if (sessionObject.has("error_message")) {
					// some error occurred
					sessionTokens.put("success", false);
					sessionTokens.put("errorMsg", sessionObject.getString("error_message"));
				}
			} else {
				sessionTokens.put("success", false);
				sessionTokens.put("errorMsg", Constants.MSG_ERROR_UNKNOWN);
			}
		} catch (JSONException je) {
			// json parse failed
			sessionTokens.put("success", false);
			sessionTokens.put("errorMsg", Constants.MSG_ERROR_UNKNOWN);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// log("Error:: success: " + sessionTokens.get("success") + "; msg: " +
		// sessionTokens.get("errorMsg"));
		if (sessionTokens.containsKey("errorMsg")) {
			Utils.createInfoDialog(this, "", (String) sessionTokens.get("errorMsg")).show();
		} else {
			Utils.createInfoDialog(this, "", Constants.MSG_ERROR_UNKNOWN).show();
		}
	}

	private boolean validateFields() {
		if (!Utils.isValidString(un, Constants.USERNAME_MIN_LENGTH, Constants.USERNAME_MAX_LENGTH)) {
			Utils.createInfoDialog(this, "", Constants.MSG_ERROR_INVALID_USERNAME).show();
			return false;
		}
		if (!Utils.isValidString(pw, Constants.PASSWORD_MIN_LENGTH, Constants.PASSWORD_MAX_LENGTH)) {
			Utils.createInfoDialog(this, "", Constants.MSG_ERROR_INVALID_PASSWORD).show();
			return false;
		}
		return true;
	}

	private void handleRememberBtn(int id, String u, String em, String ak, String fn, String ln) {
		CheckBox rm = (CheckBox) findViewById(R.id.remember_me_btn);
		if (rm.isChecked()) {
			// remember login
			log("remembering info");
			Editor editor = mPreferences.edit().putInt("id", id).putString("user_name", u).putString("em", em).putString("api_key", ak)
					.putString("fn", fn).putString("ln", ln);
			editor.commit();
		} else {
			// un-remember login
			log("un-remembering info");
			mPreferences.edit().clear().commit();
		}
	}

	private void log(String msg) {
		Utils.Logger.error("Login Activity", msg);
	}

	/**************** Button handlers *******************/
	public void loginClicked(View v) {
		log("Login clicked");
		un = ((EditText) findViewById(R.id.username_field)).getText().toString();
		pw = ((EditText) findViewById(R.id.password_field)).getText().toString();
		authenticate();
	}

	public void showSignup(View v) {
		Intent i = new Intent(this, SignupActivity.class);
		startActivityForResult(i, Constants.REQUEST_CODE_SIGNUP);
	}

	public void forgotPasswordClicked(View v) {
		Intent i = new Intent(this, ForgotPasswordActivity.class);
		startActivity(i);
	}
}
