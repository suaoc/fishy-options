package com.aoc.android.fo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.widget.Toast;

public class VotesPoster extends AsyncTask<String, Void, String> {

	private CustomSpinner cSpinner = null;
	private NetworkActivity nActivity = null;
	private JSONObject userObject = null;
	private String errorMsg = null;
	private boolean success = true;

	public VotesPoster(NetworkActivity ba, JSONObject uo, boolean get) {
		this.nActivity = ba;
		this.userObject = uo;
		this.success = get;
	}

	@Override
	protected void onPreExecute() {
		// -- check internet connection
		if (!Utils.checkInternet(nActivity.getContext())) {
			Toast.makeText(nActivity.getContext(), Constants.MSG_NO_INTERNET, Toast.LENGTH_SHORT).show();
			this.cancel(true);
			return;
		}
		cSpinner = CustomSpinner.show(nActivity.getContext(), "", "Please wait...", true, false, null);
	}

	@Override
	protected void onPostExecute(String result) {
		dismissSpinner();
		if (result != null) {
			nActivity.parseResponse(result);
		} else {
			log("Response is null");
			if (errorMsg != null) {
				Utils.createInfoDialog(nActivity.getContext(), "", errorMsg).show();
				errorMsg = null;
			}
		}
	}

	@Override
	protected String doInBackground(String... params) {
		if (this.success) {
			return doGet(params[0]);
		} else {
			return doPost(params[0]);
		}
	}

	private String doGet(String url) {
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet(url);
			HttpResponse response = client.execute(request);
			// Here i try to read the response
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			final StringBuilder sb = new StringBuilder();
			final String sep = System.getProperty("line.separator");
			String line = "";
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(sep);
			}
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String doPost(String url) {
		String response = null;
		DefaultHttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		try {
			if (userObject != null) {
				StringEntity se = new StringEntity(userObject.toString());
				post.setEntity(se);
			}
			post.setHeader("Content-Type", "application/json");
			post.setHeader("Accept", "application/json");
		} catch (UnsupportedEncodingException uee) {
			uee.printStackTrace();
		}

		try {
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			response = client.execute(post, responseHandler);
			log("Response is: " + response);
		} catch (HttpResponseException hre) {
			if (hre.getStatusCode() == 401) {
				errorMsg = Constants.MSG_ERROR_UNAUTHORIZED;
			}
		} catch (ClientProtocolException cpe) {
			cpe.printStackTrace();
			response = null;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			log("IOException occurred, returning..");
			response = null;
		}
		return response;
	}

	private void dismissSpinner() {
		if (cSpinner != null && cSpinner.isShowing()) {
			cSpinner.dismiss();
		}
	}

	private void log(String msg) {
		Utils.Logger.error("BackgroundWorker", msg);
	}

}
