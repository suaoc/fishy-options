package com.aoc.android.fo;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;

import com.google.gson.Gson;

public class JSonParser {
	public TradeData runJSONParser(String url) {

		try {
			FishyOptionsActivity.log("Json Parser", "Json Parser started..");
			Gson gson = new Gson();
			Reader r = new InputStreamReader(getJSONData(Constants.URL_BASE
					+ url));
			FishyOptionsActivity
					.log("Json parser Url", Constants.URL_BASE + url);
			Log.i("MY INFO", r.toString());
			TradeData tradeObj = gson.fromJson(r, TradeData.class);
			Log.i("MY INFO", "" + tradeObj.getObjects().size());
			

			return tradeObj;
		} catch (Exception ex) {

			ex.printStackTrace();
			return null;
		}
	}

	public InputStream getJSONData(String url) {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		URI uri;
		InputStream data = null;
		try {
			uri = new URI(url);
			HttpGet method = new HttpGet(uri);
			HttpResponse response = httpClient.execute(method);
			data = response.getEntity().getContent();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}
}
