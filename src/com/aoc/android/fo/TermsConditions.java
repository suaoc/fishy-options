package com.aoc.android.fo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class TermsConditions extends Activity implements OnClickListener {

	Button agree;
	Button dagree;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.terms);
		agree = (Button) findViewById(R.id.agree);
		dagree = (Button) findViewById(R.id.dagree);
		agree.setOnClickListener(this);
		dagree.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == agree.getId()) {

			Intent i = new Intent(TermsConditions.this, FishyOptionsActivity.class);
			startActivity(i);
			this.finish();

		} else if (v.getId() == dagree.getId()) {
			Utils.createConfirmExitDialog(this).show();
		}
	}

}
