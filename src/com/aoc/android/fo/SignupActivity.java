package com.aoc.android.fo;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SignupActivity extends Activity implements NetworkActivity {

	String un = null, pw = null, cpw = null, fn = null, ln = null, em = null;
	HashMap<String, Object> sessionTokens = null;
	EditText usernameField = null, passwordField = null, confirmPasswordField = null, emailField = null,
			firstNameField = null, lastNameField = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup);

		usernameField = ((EditText) findViewById(R.id.username_field));
		passwordField = ((EditText) findViewById(R.id.pwd_field));
		confirmPasswordField = ((EditText) findViewById(R.id.confirm_pwd_field));
		emailField = ((EditText) findViewById(R.id.email_field));
		firstNameField = ((EditText) findViewById(R.id.first_name_field));
		lastNameField = ((EditText) findViewById(R.id.last_name_field));
		// Button signUpBtn = (Button) findViewById(R.id.btn_create_account);
	}

	@Override
	public Context getContext() {
		return this;
	}

	private void authenticate() {
		if (!this.validateFields()) {
			return;
		}

		JSONObject userObj = new JSONObject();
		try {
			userObj.put("username", un);
			userObj.put("password", pw);
			userObj.put("email", em);
			userObj.put("first_name", fn);
			userObj.put("last_name", ln);
		} catch (JSONException je) {
			je.printStackTrace();
		}
		BackgroundWorker bw = new BackgroundWorker(this, userObj, false, true);
		bw.execute(Constants.URL_BASE + Constants.URL_SIGN_UP);
	}

	public void parseResponse(String jsonResponse) {
		sessionTokens = new HashMap<String, Object>();
		try {
			if (jsonResponse != null) {
				JSONObject sessionObject = new JSONObject(jsonResponse);
				if (sessionObject.has("success")) {
					boolean success = sessionObject.getBoolean("success");
					if (success) {
						sessionTokens.put("success", success);
						sessionTokens.put("api_key", sessionObject.getString("api_key"));
						sessionTokens.put("user_name", sessionObject.getString("username"));
						sessionTokens.put("id", sessionObject.getInt("id"));
						sessionTokens.put("first_name", sessionObject.getString("first_name"));
						sessionTokens.put("last_name", sessionObject.getString("last_name"));
						sessionTokens.put("email", sessionObject.getString("email"));
						log(sessionTokens.get("user_name") + "; " + sessionTokens.get("api_key") + "; "
								+ sessionTokens.get("first_name"));
						Toast.makeText(this, "Account created!", Toast.LENGTH_SHORT).show();

						LoginActivity.sessionTokens.clear();
						LoginActivity.sessionTokens.putAll(sessionTokens);
						setResult(Constants.RESULT_CODE_SIGNUP_SUCCESS);
						this.finish();
						return;
					} else {
						// signup failed due to some reason
						log(sessionObject.getString("reason"));
						sessionTokens.put("errorMsg", sessionObject.getString("reason"));
					}
				} else if (sessionObject.has("error_message")) {
					// some error occurred
					log(sessionObject.getString("error_message"));
					sessionTokens.put("success", false);
					sessionTokens.put("errorMsg", sessionObject.getString("error_message"));
				}
			} else {
				sessionTokens.put("success", false);
				sessionTokens.put("errorMsg", "An error occurred!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (sessionTokens.containsKey("errorMsg")) {
			Utils.createInfoDialog(this, "", (String) sessionTokens.get("errorMsg")).show();
		} else {
			Utils.createInfoDialog(this, "", Constants.MSG_ERROR_UNKNOWN).show();
		}
	}

	private boolean validateFields() {
		int unMin = 3, unMax = 20, pwMin = 4, pwMax = 15;

		if (!Utils.isValidString(un, unMin, unMax)) {
			Utils.createInfoDialog(this, "", Constants.MSG_ERROR_INVALID_USERNAME).show();
			return false;
		}
		if (!Utils.isValidString(pw, pwMin, pwMax)) {
			log(Constants.MSG_ERROR_INVALID_PASSWORD);
			Utils.createInfoDialog(this, "", Constants.MSG_ERROR_INVALID_PASSWORD).show();
			return false;
		}
		if (!pw.equals(cpw)) {
			log("Passwords do not match!");
			Utils.createInfoDialog(this, "", "Passwords do not match!").show();
			return false;
		}
		if (Utils.isEmptyString(fn)) {
			log("Please enter a valid first name!");
			Utils.createInfoDialog(this, "", "Please enter a valid first name").show();
			return false;
		}

		if (Utils.isEmptyString(ln)) {
			log("Please enter a valid last name");
			Utils.createInfoDialog(this, "", "Please enter a valid last name").show();
			return false;
		}
		if (Utils.isEmptyString(em) || !Utils.isValidEmail(em)) {
			log(Constants.MSG_ERROR_INVALID_EMAIL);
			Utils.createInfoDialog(this, "", Constants.MSG_ERROR_INVALID_EMAIL).show();
			return false;
		}
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	public void signupClicked(View v) {
		un = usernameField.getText().toString();
		pw = passwordField.getText().toString();
		cpw = confirmPasswordField.getText().toString();
		fn = firstNameField.getText().toString();
		ln = lastNameField.getText().toString();
		em = emailField.getText().toString();
		authenticate();
	}

	private void log(String msg) {
		Utils.Logger.error("Signup", msg);
	}

}
