package com.aoc.android.fo;

import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ForgotPasswordActivity extends Activity implements NetworkActivity {

	EditText usernameField = null, emailField = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forgot_password);

		usernameField = (EditText) findViewById(R.id.username_field);
		emailField = (EditText) findViewById(R.id.email_field);
	}

	/**************** Button handlers *******************/

	public void recoverPasswordClicked(View v) {
		// {"username":"amer", "email":"amertahir@gmail.com"}'
		// http://fishyoptionsapp.appspot.com/api/v1/user/forgotpassword/?format=json
		String username = usernameField.getText().toString();
		String email = emailField.getText().toString().toLowerCase(Locale.US);

		if (!Utils.isValidString(username, Constants.USERNAME_MIN_LENGTH, Constants.USERNAME_MAX_LENGTH)) {
			Utils.createInfoDialog(this, "", Constants.MSG_ERROR_INVALID_USERNAME).show();
			return;
		}

		if (Utils.isEmptyString(email) || !Utils.isValidEmail(email)) {
			log(Constants.MSG_ERROR_INVALID_EMAIL);
			Utils.createInfoDialog(this, "", Constants.MSG_ERROR_INVALID_EMAIL).show();
			return;
		}

		JSONObject uo = new JSONObject();
		try {
			uo.put("username", username);
			uo.put("email", email);
		} catch (JSONException je) {
			je.printStackTrace();
		}
		BackgroundWorker bw = new BackgroundWorker(this, uo, false, true);
		bw.execute(Constants.URL_BASE + Constants.URL_FORGOT_PASSWORD);
	}

	@Override
	public Context getContext() {
		return this;
	}

	@Override
	public void parseResponse(String jsonResponse) {
		log(jsonResponse);

		JSONObject sessionObject;
		try {
			sessionObject = new JSONObject(jsonResponse);

			if (sessionObject.has("success")) {
				boolean success = sessionObject.getBoolean("success");
				if (success) {
					// email has been sent to user
					Toast.makeText(this, Constants.MSG_EMAIL_SENT, Toast.LENGTH_SHORT).show();
					this.finish();
					return;
				} else {
					Toast.makeText(this, "Incorrect username or email!", Toast.LENGTH_SHORT).show();
					return;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
			Toast.makeText(this, Constants.MSG_ERROR_UNKNOWN, Toast.LENGTH_SHORT).show();
		}
	}

	private void log(String msg) {
		Utils.Logger.error("Forgot Password", msg);
	}
}
