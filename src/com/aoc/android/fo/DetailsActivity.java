package com.aoc.android.fo;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.Time;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.aoc.android.fo.objects.Option;
import com.aoc.android.fo.objects.YFinanceObj;

public class DetailsActivity extends Activity {
	private ExpandableListView mExpandableList;
	LayoutParams lpfw = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
	LinearLayout parentLayout = null;
	HashMap<String, YFDObj> objs = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);

		parentLayout = (LinearLayout) findViewById(R.id.details_parent);

		//////
		boolean fromMyList = getIntent().getExtras().getBoolean("fromMyList");
		List<Option> options = null;
		if (!fromMyList) {
			log("selected index is: " + FishyOptionsActivity.selectedItem);
			log("options size is: " + FishyOptionsActivity.tdo.objects.size());
			if (FishyOptionsActivity.selectedItem >= FishyOptionsActivity.tdo.objects.size()) {
				log("index out of bounds for tdo");
				return;
			}
			options = FishyOptionsActivity.tdo.objects.get(FishyOptionsActivity.selectedItem).getOptions();
		} else {
			log("selected index is: " + MyListActivity.selectedItem);
			log("options size is: " + MyListActivity.mld.objects.size());
			if (FishyOptionsActivity.selectedItem >= MyListActivity.mld.objects.size()) {
				log("index out of bounds for mld");
				return;
			}
			options = MyListActivity.mld.objects.get(MyListActivity.selectedItem).getTrade().getOptions();
		}
		if (options == null) {
			return;
		}
		//////////
		if (options.size() < 1) {
			showNoDataFound();
			log("options size is less than 1");
		}
		for (Option option : options) {
			log("Figures for " + option.symbol);
			if (option.figures == null || option.figures.size() <= 0) {
				continue;
			}
			addTitle(option.symbol);
			addRow("Date", "Low", "High", "Vol", "Open Int.", true);
			log("Figures size: " + option.figures.size());
			for (YFinanceObj yfo : option.figures) {
				// log("high is " + yfo.high);
				// log("low is " + yfo.low);
				// log("volume is " + yfo.vol);
				// log("oi is " + yfo.oi);
				addRow(yfo.date, yfo.low + "", yfo.high + "", yfo.vol + "", yfo.oi + "", false);
			}
		}
	}

	public void displayData() {
		if (objs == null) {
			log("objs null");
			return;
		}
		Time time = new Time(Time.getCurrentTimezone());
		time.setToNow();
		String dt = time.year + "." + time.month + "." + time.monthDay;
		for (String key : objs.keySet()) {
			YFDObj yo = objs.get(key);
			addTitle(key);
			addRow("Date", "Low", "High", "Vol", "Open Int.", true);
			addRow(dt, yo.lo, yo.hi, yo.vo, yo.oi, false);
		}
	}

	private void addTitle(String title) {
		TextView t = new TextView(this);
		t.setText(title);
		t.setTextColor(getResources().getColor(R.color.black));
		log(title);
		t.setGravity(Gravity.CENTER);
		lpfw.setMargins(0, 20, 0, 0);
		parentLayout.addView(t, lpfw);
	}

	private void showNoDataFound() {
		parentLayout.removeAllViews();
		TextView t = new TextView(this);
		t.setText("No data found!");
		log("No Data Found");
		t.setGravity(Gravity.CENTER);
		LayoutParams lpfw = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		lpfw.setMargins(0, 20, 0, 0);
		parentLayout.addView(t, lpfw);
	}

	private void addRow(String d, String l, String h, String v, String oi, boolean header) {

		// TODO if header, then change background color to something else
		LayoutParams lpfw = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		LayoutInflater li = LayoutInflater.from(this);

		LinearLayout row = (LinearLayout) li.inflate(R.layout.list_item_child, null);
		((TextView) row.findViewById(R.id.tv_details_date)).setText(d);
		((TextView) row.findViewById(R.id.tv_details_low)).setText(l);
		((TextView) row.findViewById(R.id.tv_details_high)).setText(h);
		((TextView) row.findViewById(R.id.tv_details_vol)).setText(v);
		((TextView) row.findViewById(R.id.tv_details_open_int)).setText(oi);

		parentLayout.addView(row, lpfw);
	}

	// private void createUI() {
	// mExpandableList = (ExpandableListView)
	// findViewById(R.id.expandable_list);
	//
	// ArrayList<Parent> arrayParents = new ArrayList<Parent>();
	// ArrayList<String> arrayChildren = new ArrayList<String>();
	//
	// List<Option> options = ((Trade) tradeObj.objects.get(0)).getOptions();
	//
	// for (Option option : options) {
	// Parent parent = new Parent();
	// parent.setTitle(option.symbol);
	// arrayChildren.add("Child ");
	// parent.setArrayChildren(arrayChildren);
	// arrayParents.add(parent);
	// }
	// mExpandableList.setAdapter(new MyCustomAdapter(this, arrayParents));
	// }

	private void log(String msg) {
		Utils.Logger.error("OptionsDetails", msg);
	}

	class YFDObj {
		String hi = null, lo = null, vo = null, oi = null;

		public YFDObj(String h, String l, String v, String o) {
			hi = h;
			lo = l;
			vo = v;
			oi = o;
		}
	}

	/* Fetches data from yahoo finance */
	private class DataFetcher extends AsyncTask<Void, Void, Boolean> {
		List<Option> options = null;
		ProgressHandler pHandler = null;
		String errorMsg = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pHandler = new ProgressHandler(DetailsActivity.this);
			pHandler.sendEmptyMessage(Constants.SHOW_DIALOG);
			options = FishyOptionsActivity.tdo.objects.get(FishyOptionsActivity.selectedItem).getOptions();
			objs = new HashMap<String, DetailsActivity.YFDObj>();
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			displayData();
			pHandler.sendEmptyMessage(Constants.DISMISS_DIALOG);
		}

		private void getData(String sym) {
			Document doc;
			try {
				// String sym = "MSFT140118C00020000";
				doc = Jsoup.connect("http://finance.yahoo.com/q?s=" + sym).get();
				String dayLowIdSel = "#yfs_g00_" + sym.toLowerCase();
				String dayHighIdSel = "#yfs_h00_" + sym.toLowerCase();
				String volSel = "#yfs_v00_" + sym.toLowerCase();
				String openIntSel = "#table2 tbody > tr + tr + tr + tr";

				final String low = doc.select(dayLowIdSel).text().toString();
				final String hi = doc.select(dayHighIdSel).text().toString();
				final String vol = doc.select(volSel).text().toString();
				final String t = doc.select(openIntSel).text().toString();
				final String openInt = t.substring(t.indexOf(": ") + 2);

				objs.put(sym, new YFDObj(hi, low, vol, openInt));
				log("low:" + low + "\nhigh:" + hi + "\nvolume:" + vol + "\nOpen Int: " + openInt);
				errorMsg = null;

			} catch (SocketTimeoutException ste) {
				errorMsg = "An error occurred";
				ste.printStackTrace();
			} catch (IOException e) {
				errorMsg = "An error occurred";
				e.printStackTrace();
			} catch (Exception e) {
				errorMsg = "An error occurred";
				e.printStackTrace();
			}
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			for (final Option option : options) {
				getData(option.symbol);
				// if (errorMsg != null) {
				// // some error occurred for this option, but we continue
				// }
			}

			return null;
		}
	}
}