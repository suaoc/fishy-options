package com.aoc.android.fo;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.format.Time;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aoc.android.fo.objects.Trade;
import com.aoc.android.fo.objects.Vote;

public class VotesActivity extends Activity implements NetworkActivity, OnClickListener {

	Trade currentTrade = null;
	List<Vote> votes = null;
	VotesAdapter va = null;
	ListView lv = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_votes);

		ImageButton tu = (ImageButton) findViewById(R.id.btn_thumbs_up);
		ImageButton td = (ImageButton) findViewById(R.id.btn_thumbs_down);
		Button showAllVotes = (Button) findViewById(R.id.btn_show_all);
		Button filterUpVotes = (Button) findViewById(R.id.btn_filter_thumbs_up);
		Button filterDownVotes = (Button) findViewById(R.id.btn_filter_thumbs_down);
		tu.setOnClickListener(this);
		td.setOnClickListener(this);
		showAllVotes.setOnClickListener(this);
		filterUpVotes.setOnClickListener(this);
		filterDownVotes.setOnClickListener(this);

		// TODO
		currentTrade = FishyOptionsActivity.tdo.getObjects().get(FishyOptionsActivity.selectedItem);

		boolean fromMyList = getIntent().getExtras().getBoolean("fromMyList");
		if (!fromMyList) {
			log("selected index is: " + FishyOptionsActivity.selectedItem);
			log("options size is: " + FishyOptionsActivity.tdo.objects.size());
			if (FishyOptionsActivity.selectedItem >= FishyOptionsActivity.tdo.objects.size()) {
				log("index out of bounds for tdo");
				return;
			}
			currentTrade = FishyOptionsActivity.tdo.getObjects().get(FishyOptionsActivity.selectedItem);
		} else {
			log("selected index is: " + MyListActivity.selectedItem);
			log("options size is: " + MyListActivity.mld.objects.size());
			if (FishyOptionsActivity.selectedItem >= MyListActivity.mld.objects.size()) {
				log("index out of bounds for mld");
				return;
			}
			currentTrade = FishyOptionsActivity.tdo.getObjects().get(FishyOptionsActivity.selectedItem);
		}
		if (currentTrade == null) {
			return;
		}

		votes = currentTrade.getVotes();
		if (votes.size() <= 0) {
			((TextView) findViewById(R.id.tv_no_votes)).setVisibility(View.VISIBLE);
		}
		if (votes != null) {
			va = new VotesAdapter(this, votes, null);
			lv = (ListView) findViewById(R.id.listview_votes);
			lv.setAdapter(va);
		}
	}

	Vote newVote = null;

	private void postVote(boolean thumbsUp, String comment) {
		if (LoginActivity.sessionTokens == null) {
			Toast.makeText(this, "Unable to post. Please try again later.", Toast.LENGTH_SHORT).show();
			return;
		}
		Time time = new Time(Time.getCurrentTimezone());
		time.setToNow();
		String dt = time.year + "-" + time.month + "-" + time.monthDay + "T" + time.hour + ":" + time.minute + ":"
				+ time.second;
		JSONObject userObj = new JSONObject();
		try {
			userObj.put("comments", comment);
			userObj.put("thumbs_up", thumbsUp); // false for thumb down
			userObj.put("trade", "/api/v1/trade/" + currentTrade.getId() + "/");
			userObj.put("user", "/api/v1/user/" + LoginActivity.sessionTokens.get("id") + "/");
			userObj.put("voted_on", dt);
			log("Posting: " + userObj.toString());
			newVote = new Vote((String) LoginActivity.sessionTokens.get("user_name"), comment, thumbsUp, dt);
		} catch (JSONException je) {
			je.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		BackgroundWorker bw = new BackgroundWorker(this, userObj, false, false);
		bw.execute(Constants.URL_VOTE + "?username=" + LoginActivity.sessionTokens.get("user_name") + "&api_key="
				+ LoginActivity.sessionTokens.get("api_key"));
	}

	@Override
	public Context getContext() {
		return this;
	}

	@Override
	public void parseResponse(String jsonResponse) {
		// this is after posting vote. jsonResponse may be null
		// we only add the vote locally here.
		log("adding vote locally");
		if (newVote != null && votes != null) {
			votes.add(newVote);
			if (va != null && lv != null) {
				log("notifying data set changed");
				va = new VotesAdapter(this, votes, null);
				lv.setAdapter(va);
			}
		}
	}

	private void log(String msg) {
		Utils.Logger.error("VotesActivity", msg);
	}

	private void showCommentDialog(final boolean thumbsUp) {
		final Dialog d = new Dialog(this, R.style.CustomDialog);
		d.setContentView(R.layout.dialog_comment);
		final EditText commentField = (EditText) d.findViewById(R.id.comment_field);
		Button btnPost = (Button) d.findViewById(R.id.btn_post);
		btnPost.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String input = commentField.getText().toString();
				if (Utils.isEmptyString(input)) {
					Toast.makeText(VotesActivity.this, "Please enter a comment!", Toast.LENGTH_SHORT).show();
					return;
				}
				// post vote
				postVote(thumbsUp, input);
				d.dismiss();
			}
		});
		Button btnCancel = (Button) d.findViewById(R.id.btn_cancel);
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				d.dismiss();
			}
		});
		d.show();
	}

	private void filterList(Boolean upVotes) {
		VotesAdapter va = new VotesAdapter(this, currentTrade.getVotes(), upVotes);
		ListView lv = (ListView) findViewById(R.id.listview_votes);
		lv.setAdapter(va);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_thumbs_up:
			showCommentDialog(true);
			break;
		case R.id.btn_thumbs_down:
			showCommentDialog(false);
			break;
		case R.id.btn_show_all:
			filterList(null);
			break;
		case R.id.btn_filter_thumbs_up:
			filterList(true);
			break;
		case R.id.btn_filter_thumbs_down:
			filterList(false);
			break;
		default:
			break;
		}
	}
}
