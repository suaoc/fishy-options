package com.aoc.android.fo;

public class ResolveMonth {

	public static String resolve(int month)
	{
		String ret=null;
		switch (month) {
		case 1:
			ret="Jan";
			break;
		case 2:
			ret="Feb";
			break;
		case 3:
			ret="Mar";
			break;
		case 4:
			ret="Apr";
			break;
		case 5:
			ret="May";
			break;
		case 6:
			ret="Jun";
			break;
		case 7:
			ret="Jul";
			break;
		case 8:
			ret="Aug";
			break;
		case 9:
			ret="Sep";
			break;
		case 10:
			ret="Oct";
			break;
		case 11:
			ret="Nov";
			break;
		case 12:
			ret="Dec";
			break;

		default:
			break;
		}
		return ret;
		
	}
}
