package com.aoc.android.fo;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class OptionDetailTabs extends TabActivity {

	// Divide 1.0 by # of tabs needed, so 0.33f
	private static final LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,
			LayoutParams.WRAP_CONTENT, 0.33f);

	private static TabHost tabHost;
	private static LayoutInflater inflater;

	private View tab;
	private TextView tabLabel;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tabs_host);

		// Get inflator so we can start creating the custom view for tab
		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// Get tab manager
		tabHost = getTabHost();

		Intent intent = new Intent(OptionDetailTabs.this, DetailsActivity.class);
		intent.putExtra("fromMyList", getIntent().getExtras().getBoolean("fromMyList"));
		tabHost.addTab(createTab("0", "Details", intent));

		// Add another tab
		intent = new Intent(OptionDetailTabs.this, HighLowActivity.class);
		intent.putExtra("fromMyList", getIntent().getExtras().getBoolean("fromMyList"));
		tabHost.addTab(createTab("1", "Highest/Lowest", intent));

		// Add another tab
		intent = new Intent(OptionDetailTabs.this, VotesActivity.class);
		intent.putExtra("fromMyList", getIntent().getExtras().getBoolean("fromMyList"));
		tabHost.addTab(createTab("2", "Votes", intent));

		tabHost.setCurrentTab(0);
		// Listener to detect when a tab has changed.
		tabHost.setOnTabChangedListener(new OnTabChangeListener() {
			@Override
			public void onTabChanged(String tag) {
				// reset some styles
				clearTabStyles();
				View tabView = null;
				// Use the "tag" for the tab spec to determine which tab is
				// selected
				if (tag.equals("0")) {
					tabView = getTabWidget().getChildAt(0);
				} else if (tag.equals("1")) {
					tabView = getTabWidget().getChildAt(1);
				} else if (tag.equals("2")) {
					tabView = getTabWidget().getChildAt(2);
				} else {
					Log.e("TabsContainer", "Tab tag is wrong or not set..");
					return;
				}
				tabView.setBackgroundResource(R.drawable.orange_tab);
				// ((TextView)
				// tabView.findViewById(R.id.tabLabel)).setTextColor(Color.BLACK);
			}
		});
	}

	private TabSpec createTab(String tag, String labelStr, Intent intent) {
		tab = inflater.inflate(R.layout.tab, getTabWidget(), false);
		if (tag.equals("0")) {
			tab.setBackgroundResource(R.drawable.orange_tab);
			// ((TextView)
			// tab.findViewById(R.id.tabLabel)).setTextColor(Color.BLACK);
		}
		tab.setLayoutParams(params);
		tabLabel = (TextView) tab.findViewById(R.id.tabLabel);
		tabLabel.setText(labelStr);
		return tabHost.newTabSpec(tag).setIndicator(tab).setContent(intent);
	}

	private void clearTabStyles() {
		for (int i = 0; i < getTabWidget().getChildCount(); i++) {
			tab = getTabWidget().getChildAt(i);
			// tab.findViewById(R.id.tabSelectedDivider).setVisibility(View.GONE);
			tab.setBackgroundResource(R.drawable.blue_btn);
		}
	}

}