package com.aoc.android.fo;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

public class ProgressHandler extends Handler {
	CustomSpinner cs = null;
	Context context = null;

	public ProgressHandler(Context c) {
		cs = new CustomSpinner(c);
		context = c;
	}

	@Override
	public void handleMessage(Message msg) {
		super.handleMessage(msg);
		switch (msg.what) {
		case Constants.SHOW_DIALOG:
			if (cs == null) {
				cs = new CustomSpinner(context);
			}
			cs = CustomSpinner.show(context, "", Constants.MSG_PLZ_WAIT, true, false, null);
			break;
		case Constants.DISMISS_DIALOG:
			if (cs != null) {
				cs.dismiss();
			}
			break;

		default:
			break;
		}
	}
}
