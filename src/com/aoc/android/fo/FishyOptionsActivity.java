package com.aoc.android.fo;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

import com.aoc.android.fo.ListAdapterTrade.Tag;
import com.aoc.android.fo.objects.Trade;
import com.google.gson.Gson;

public class FishyOptionsActivity extends Activity implements OnClickListener, NetworkActivity {
	/** Called when the activity is first created. */
	public static TradeData tdo = null;
	private TradeData copy_tdo = null;
	public static FollowingData fd = null;

	ListView list;
	private EditText searchBox;
	private ImageButton clear_button;
	private ImageButton searchbox_search_button;
	private ImageButton myList_button;
	private Button login;
	public static int selectedItem = -1;
	private boolean loggedIn = false;
	// private ImageButton nextButt;
	// private ImageButton prevButt;

	SharedPreferences sp = null;
	private Button loadMore;
	private TextView message;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		loadMore = (Button) findViewById(R.id.loadmore);
		message= (TextView)findViewById(R.id.message);
		loadMore.setVisibility(View.INVISIBLE);
		sp = getSharedPreferences("creds", MODE_PRIVATE);

		copy_tdo = new TradeData();

		if (tdo == null || tdo.getObjects().size() <= 0) {
			BackgroundWorker bw = new BackgroundWorker(this, null, true, true);
			bw.execute(Constants.URL_BASE + Constants.URL_TRADE);
		} else {
			this.setListAdapter();
		}
		searchBox = (EditText) findViewById(R.id.searchbox);

		searchBox.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView tv, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					doSearch();
					return true;
				}
				return false;
			}
		});
		clear_button = (ImageButton) findViewById(R.id.clear_button);
		searchbox_search_button = (ImageButton) findViewById(R.id.searchbox_searchButton);

		// nextButt = (ImageButton) findViewById(R.id.btn_next_page);
		// prevButt = (ImageButton) findViewById(R.id.btn_previous_page);

		clear_button.setOnClickListener(this);
		searchbox_search_button.setOnClickListener(this);

		login = (Button) findViewById(R.id.login);
		if (checkLogin()) {
			loggedIn = true;
			login.setText("Settings");
		} else {
			log("Not logged in");
		}
		login.setOnClickListener(this);

		Button newToFishy = (Button) findViewById(R.id.newuser);
		newToFishy.setOnClickListener(this);
		InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(searchBox.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

		loadMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				log("loading more");
				if (tdo != null)
					if (tdo.getMeta().next != null || !tdo.getMeta().next.equals("")) {
						BackgroundWorker bwc = new BackgroundWorker(FishyOptionsActivity.this, null, true, true);
						bwc.execute(Constants.URL_BASE + tdo.getMeta().next);

					}
			}
		});

	}

	@Override
	public void finish() {

		tdo = null;
		super.finish();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == Constants.REQUEST_CODE_LOGIN && resultCode == Constants.RESULT_CODE_LOGIN_SUCCESS) {
			// e.g. change 'login' to 'Logged In'
			login.setText("Settings");
			this.loggedIn = true;
			return;
		}
		if (resultCode == Constants.RESULT_CODE_LOGOUT) {
			logout();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void myClickHandler(View v) {

		if (!loggedIn) {
			Intent i = new Intent(FishyOptionsActivity.this, LoginActivity.class);
			startActivityForResult(i, Constants.REQUEST_CODE_LOGIN);
			return;
		}
		ImageButton b = (ImageButton) v;
		JSONObject postData = new JSONObject();
		try {
			log(((Tag) b.getTag()).tradeName);
			postData.put("trade", ((Tag) b.getTag()).tradeName);
			postData.put("user", "/api/v1/user/" + LoginActivity.sessionTokens.get("id") + "/");
			BackgroundWorker bw = new BackgroundWorker(this, postData, false, false);
			bw.execute(Constants.URL_Follow + "?username=" + LoginActivity.sessionTokens.get("user_name") + "&api_key="
					+ LoginActivity.sessionTokens.get("api_key"));

		} catch (JSONException e) {
			e.printStackTrace();

		}

	}

	public void doSearch() {
		copy_tdo.setMeta(tdo.getMeta());
		copy_tdo.setObjects(tdo.getObjects());
		tdo = null;
		BackgroundWorker bw = new BackgroundWorker(FishyOptionsActivity.this, null, true, true);
		String text = searchBox.getText().toString().trim();
		log(text + "this is text");
		if (text != null && !text.equals("")) {
			String searchUrl = String.format(Constants.URL_SEARCH, text.toUpperCase(Locale.getDefault()));
			log(searchUrl);
			bw.execute(searchUrl);
		}

	}

	public void setListAdapter() {
		list = (ListView) findViewById(R.id.list);
		ArrayList<Trade> trades = tdo.getObjects();
		if (trades == null) {
			log("trade is null");
		}
		list.setAdapter(new ListAdapterTrade(this, trades));

		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
				if (!loggedIn) {
					// Toast.makeText(FishyOptionsActivity.this,
					// "Please login first", Toast.LENGTH_SHORT).show();
					Intent i = new Intent(FishyOptionsActivity.this, LoginActivity.class);
					startActivityForResult(i, Constants.REQUEST_CODE_LOGIN);
					return;
				}
				log("clicking item");
				selectedItem = pos;
				Intent i = new Intent(FishyOptionsActivity.this, OptionDetailTabs.class);
				i.putExtra("fromMyList", false);
				startActivity(i);
			}
		});

		if (tdo.getMeta() != null) {

			// the other navigation system
			/*
			 * if (nextButt == null) { nextButt = (ImageButton)
			 * findViewById(R.id.btn_next_page); } if (prevButt == null) {
			 * prevButt = (ImageButton) findViewById(R.id.btn_previous_page); }
			 * nextButt.setVisibility(View.VISIBLE); if (tdo.getMeta().next !=
			 * null) { nextButt.setOnClickListener(new OnClickListener() {
			 * 
			 * @Override public void onClick(View v) {
			 * 
			 * tdo.setObjects(null); BackgroundWorker bw = new
			 * BackgroundWorker(FishyOptionsActivity.this, null, true, true);
			 * bw.execute(Constants.URL_BASE + tdo.getMeta().next); } }); } else
			 * { nextButt.setVisibility(View.INVISIBLE);
			 * 
			 * } if (tdo.getMeta().previous != null) {
			 * prevButt.setVisibility(View.VISIBLE);
			 * prevButt.setOnClickListener(new OnClickListener() {
			 * 
			 * @Override public void onClick(View v) {
			 * 
			 * tdo.setObjects(null); BackgroundWorker bw = new
			 * BackgroundWorker(FishyOptionsActivity.this, null, true, true);
			 * bw.execute(Constants.URL_BASE + tdo.getMeta().previous); } }); }
			 * else { prevButt.setVisibility(View.INVISIBLE);
			 * 
			 * }
			 */

			if (tdo.getMeta().next == null) {
				loadMore.setVisibility(View.INVISIBLE);
			} else
				loadMore.setVisibility(View.VISIBLE);
		}

		myList_button = (ImageButton) findViewById(R.id.mylist);
		myList_button.setOnClickListener(this);

	}

	public static void log(String message) {

		Log.e("Fishy Options Activity", message);
	}

	public static void log(String source, String message) {

		Log.e(source, message);
	}

	public void setTradeData(TradeData tdo) {
		FishyOptionsActivity.tdo = tdo;
	}

	private void hideSearchUI() {
		// hide keyboard
		InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(searchBox.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		searchBox.setText("");
		message.setText("");
		if (copy_tdo.getObjects() != null) {

			tdo = copy_tdo;
			setListAdapter();
		}
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Utils.createConfirmExitDialog(this).show();
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.searchbox_searchButton:
			doSearch();
			break;

		case R.id.clear_button:
			hideSearchUI();
			break;
		case R.id.login:
			if (!loggedIn) {
				Intent i = new Intent(FishyOptionsActivity.this, LoginActivity.class);
				startActivityForResult(i, Constants.REQUEST_CODE_LOGIN);
			} else {
				// logout();
				Intent settings = new Intent(FishyOptionsActivity.this, SettingsActivity.class);
				startActivityForResult(settings, 1);
			}
			break;
		case R.id.newuser:
			Intent i = new Intent(FishyOptionsActivity.this, NewToFishy.class);
			startActivity(i);
			break;

		case R.id.mylist:
			if (!loggedIn) {
				Intent login_intent = new Intent(FishyOptionsActivity.this, LoginActivity.class);
				startActivityForResult(login_intent, Constants.REQUEST_CODE_LOGIN);
				break;
			} else // if(fd!=null)
			{
				Intent mylist_intent = new Intent(FishyOptionsActivity.this, MyListActivity.class);
				startActivity(mylist_intent);
				break;
			}

		}

	}

	@Override
	public Context getContext() {
		return this;
	}

	@Override
	public void parseResponse(String jsonResponse) {

		try {
			FishyOptionsActivity.log("Json Parser", "Json Parser started..");
			Gson gson = new Gson();
			Reader r = new InputStreamReader(new ByteArrayInputStream(jsonResponse.getBytes()));

			// getJSONData(Constants.URL_BASE
			// + url));
			TradeData tradeObj = gson.fromJson(r, TradeData.class);
			// for (Option o : tradeObj.getObjects().get(0).getOptions()) {
			// FishyOptionsActivity.log("JsonParser", o.getSymbol());
			// }

			if (tradeObj != null) {
				// tdo = tradeObj;

				if (tdo != null) {
					for (Trade temp : tradeObj.getObjects()) {
						tdo.getObjects().add(temp);
					}
					tdo.setMeta(tradeObj.getMeta());
				} else
					tdo = tradeObj;
				for (Trade t : tdo.objects) {
					log(t.getStk(), t.getCategory());
				}
				if(tdo.objects==null || tdo.objects.size()==0)
				{
					
					//message.setText("No Results found");
					Toast.makeText(this, "No Search Results found", Toast.LENGTH_SHORT).show();
				}

				// for (Trade t : tdo.objects) {
				// List<Option> options = t.getOptions();
				// for (Option option : options) {
				// log("Figures for " + option.symbol);
				// if (option.figures == null || option.figures.size() <= 0) {
				// continue;
				// }
				// log("Figures size: " + option.figures.size());
				// for (YFinanceObj yfo : option.figures) {
				// log("high is " + yfo.high);
				// log("low is " + yfo.low);
				// log("volume is " + yfo.vol);
				// log("oi is " + yfo.oi);
				// }
				// }
				// }
				setListAdapter();

			}

		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	private boolean checkLogin() {

		try {
			if (sp.contains("id") && sp.contains("api_key") && sp.contains("user_name")) {
				LoginActivity.sessionTokens = new HashMap<String, Object>();
				LoginActivity.sessionTokens.put("id", sp.getInt("id", -1));
				LoginActivity.sessionTokens.put("api_key", sp.getString("api_key", null));
				LoginActivity.sessionTokens.put("user_name", sp.getString("user_name", null));
				LoginActivity.sessionTokens.put("first_name", sp.getString("first_name", null));
				LoginActivity.sessionTokens.put("last_name", sp.getString("last_name", null));
				LoginActivity.sessionTokens.put("email", sp.getString("email", null));
				return true;
			} else {
				log("prefs do not contain something");
			}
		} catch (Exception e) {
			// failed to load login info
			sp.edit().clear().commit();
			e.printStackTrace();
			return false;
		}
		return false;
	}

	private void logout() {
		if (sp == null) {
			return;
		}
		sp.edit().clear().commit();
		if (login == null) {
			login = (Button) findViewById(R.id.login);
		}
		login.setText("Login");
		loggedIn = false;
	}
}