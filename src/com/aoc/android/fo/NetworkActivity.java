package com.aoc.android.fo;

import android.content.Context;

public interface NetworkActivity {

	public Context getContext();

	public void parseResponse(String jsonResponse);
}
