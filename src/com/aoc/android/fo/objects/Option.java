package com.aoc.android.fo.objects;

import java.util.List;

public class Option {
	public int id;
	public int month;
	public int qty;
	public int strike;
	public int year;
	public String action;
	public String resource_uri;
	public String symbol;
	public String type;
	public Highest highest = null;
	public Lowest lowest = null;
	public List <YFinanceObj> figures = null;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public int getStrike() {
		return strike;
	}

	public void setStrike(int strike) {
		this.strike = strike;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getResource_uri() {
		return resource_uri;
	}

	public void setResource_uri(String resource_uri) {
		this.resource_uri = resource_uri;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static class Highest {
		public String date = null;
		public double high = -1;
	}

	public static class Lowest {
		public String date = null;
		public double low = -1;
	}
}
