package com.aoc.android.fo.objects;

public class Vote {

	public String comments;
	public String resource_uri;
	public String user;
	public String username;
	public String voted_on;
	public int id;
	public int userid;
	public boolean thumbs_up;

	public Vote() {
	}

	// use only for creating local votes..
	public Vote(String username, String comments, boolean thumbsUp, String date) {
		this.username = username;
		this.comments = comments;
		this.thumbs_up = thumbsUp;
		this.voted_on = date;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getResource_uri() {
		return resource_uri;
	}

	public void setResource_uri(String resource_uri) {
		this.resource_uri = resource_uri;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getVoted_on() {
		return voted_on;
	}

	public void setVoted_on(String voted_on) {
		this.voted_on = voted_on;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public boolean isThumbs_up() {
		return thumbs_up;
	}

	public void setThumbs_up(boolean thumbs_up) {
		this.thumbs_up = thumbs_up;
	}

}
