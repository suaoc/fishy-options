package com.aoc.android.fo.objects;

import java.util.List;

public class Trade {
	private String category;
	private int id;
	private boolean live;
	private String identified_on;
	private String resource_uri;
	private String stk;
	private List<Option> options;
	private List<Follower> followers;
	private List<Vote> votes;
	private int vote_down_count;
	private int vote_up_count;
	
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isLive() {
		return live;
	}
	public void setLive(boolean live) {
		this.live = live;
	}
	public String getIdentified_on() {
		return identified_on;
	}
	public void setIdentified_on(String identified_on) {
		this.identified_on = identified_on;
	}
	public String getResource_uri() {
		return resource_uri;
	}
	public void setResource_uri(String resource_uri) {
		this.resource_uri = resource_uri;
	}
	public String getStk() {
		return stk;
	}
	public void setStk(String stk) {
		this.stk = stk;
	}
	public List<Option> getOptions() {
		return options;
	}
	public void setOptions(List<Option> options) {
		this.options = options;
	}
	public List<Follower> getFollowers() {
		return followers;
	}
	public void setFollowers(List<Follower> followers) {
		this.followers = followers;
	}
	public int getVote_down_count() {
		return vote_down_count;
	}
	public void setVote_down_count(int vote_down_count) {
		this.vote_down_count = vote_down_count;
	}
	public int getVote_up_count() {
		return vote_up_count;
	}
	public void setVote_up_count(int vote_up_count) {
		this.vote_up_count = vote_up_count;
	}
	public List<Vote> getVotes() {
		return votes;
	}
	public void setVotes(List<Vote> votes) {
		this.votes = votes;
	}
	
}
